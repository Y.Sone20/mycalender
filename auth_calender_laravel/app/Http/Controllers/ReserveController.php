<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\schedule;
use App\Book;
use App\User;
use Illuminate\Validation\Rule;

class ReserveController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function reservation(){
    	$book = Book::orderBy("schedule_id")->where("user_id" , "=", Auth::id())->get();

    	$delflg = false;

    	return view('book.reservation')->with([
    		"book" => $book,
    		"delflg" => $delflg
    	]);
    }

    public function cancel(Request $request){

    	$delflg = true;

    	$book = Book::where('book_id',$request->bookID);

    	$book->delete();

    	$book = Book::orderBy("schedule_id")->where("user_id" , "=", Auth::id())->get();

    	return view('book.reservation')->with([
    		"book" => $book,
    		"delflg" => $delflg
    	]);
    }
}
