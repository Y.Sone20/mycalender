<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->validate($request,[
            'id' => 'digits:6'
        ]);
        
        $book = Book::select("schedule_id","book_time")->get();

        if (!isset($request->id)) {
            $thisYear = date('Y');
            $thisMonth = date('m');
        }else{
            $thisYear = substr($request->id,0,4);
            $thisMonth = substr($request->id,4,2);
        }

        return view('index')->with([
            "book" => $book,
            "thisYear" => $thisYear,
            "thisMonth" => $thisMonth,
        ]);
    }
}
