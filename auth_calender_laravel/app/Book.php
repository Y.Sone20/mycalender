<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    //
    protected $fillable = [
    	'book_time'
    ];

    public function user(){
    	return $this->belongsTo('App\User');
    }

    public function schedule(){
    	return $this->belongsTo('App\schedule');
    }
}
