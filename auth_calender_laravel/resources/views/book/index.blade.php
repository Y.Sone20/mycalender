@extends('layouts.app')


<!DOCTYPE html>
<html lang="ja">
<head>
	<title>Reserve</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/base.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
	<link rel="stylesheet" type="text/css" href="css/reserve.css">
</head>
<body>
@section('content')
	<div class="container">
		<h1>Reserve</h1>
		<div class="text"><p>名前と電話番号、予約時間を入力のうえ、確認画面へお進みください。<br>特記事項がある場合は備考欄に記入してください。<br><span class="err_message">
			@foreach ($errors->all() as $error)
			{{$error}}<br>
			@endforeach</span>
		</p></div>
		<section class="calender_title">
			<div class="month_body"><span class="year">{{ substr($request->id, 0, 4) }}</span>年<br><span class="month">{{ substr($request->id, 4, 2) }}</span>月<span class="month">{{ substr($request->id, 6, 2) }}</span>日</div>
		</section>
		<section class="book_form">
			<form method="post" action="{{ action('schedulesController@confirm',[$request->id]) }}">
				{{ csrf_field() }}
				<div class="time_area">
					<label class="form_label" style="margin-left: 2px;">NAME:</label>
					<input type="text" name="name" class="first" value="{{ old('name') }}" required><br>

					<label class="form_label" style="margin-left: 25px;">TEL:</label>
					<input type="tel" name="tel" class="second" value="{{ old('tel') }}" required><br>

					<label class="form_label timetitle"><span>TIME:</span></label>
					<input type="radio" name="book_time" id="11" value="11:00" required><div class="time" id="tx11"> 11:00</div>
					<input type="radio" name="book_time" id="15" value="15:00"><div class="time" style="margin-right: 0;" id="tx15"> 15:00</div><br>
					<label class="form_label">NOTE:</label>
					<textarea type="text" name="note">{{ old('note') }}</textarea>
					<input type="hidden" name="id" value="{{ $request->id }}">
				</div>

				<input type="submit" name="submit" value="確認画面へ">
				<input type="button" name="back" id="back" onClick="location.href='./home';"　value="戻る">

			</form>
		</section>
		
	</div>

	@endsection

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/button.js"></script>
	<script type="text/javascript">
		var book = @json($book);
		var books = [];

		// 多次元配列がJqueryだと扱いづらいため連想配列まで落とす。
		for (var i = 0 ; i < book.length; i++) {
			books[i] = book[i]["book_time"];
		}

		$(document).ready(function(){
			$.each(books,function(index,value){
				$('#'+value.substr(0,2)).prop("disabled", true);
				$('#tx'+value.substr(0,2)).css('opacity','0.5');
			})
			
		});
	</script>
<!-- </body>
</html> -->