@extends('layouts.app')


<!DOCTYPE html>
<html lang="ja">
<head>
	<title>Reserved!</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/base.css">
	<link rel="stylesheet" type="text/css" href="../css/index.css">
	<link rel="stylesheet" type="text/css" href="../css/confirm.css">
</head>
<body>
@section('content')
	<div class="container">
		<h1>Reserved!</h1>
		<div class="text"><p>下記内容にてご予約を承りました。</p></div>
		<section class="calender_title">
			<div class="month_body"><span class="year">{{ substr($request->id, 0, 4) }}</span>年<br><span class="month">{{ substr($request->id, 4, 2) }}</span>月<span class="month">{{ substr($request->id, 6, 2) }}</span>日</div>
		</section>
		<section class="book_form">
				<div class="time_area">
					<label class="form_label bookId">ID：</label>0000{{ $book }}<br>
					<label class="form_label one">NAME：</label>{{ $request->name }}<br>

					<label class="form_label two">TEL：</label>{{ $request->tel }}<br>

					<label class="form_label three">TIME：</label><div class="time">{{ $request->book_time }}</div><br>
					<label class="form_label four">NOTE：</label>{{ $request->note }}
				</div>

				<button name="toTop" id="toTop">TOPへ</button>
		</section>
	</div>

	@endsection

	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript" src="../js/toTop.js"></script>
<!-- </body>
</html> -->