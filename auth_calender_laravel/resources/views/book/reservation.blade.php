@extends('layouts.app')


<!DOCTYPE html>
<html lang="ja">
<head>
	<title>Reserve</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/base.css">
	<link rel="stylesheet" type="text/css" href="../css/index.css">
	<link rel="stylesheet" type="text/css" href="../css/confirm.css">
	<link rel="stylesheet" type="text/css" href="../css/reservation.css">
</head>
<body>
@section('content')
	<div class="container">
		<h1>Reservation</h1>
		<div class="text"><p>
			@if ($delflg)
			<span class="red">予約を削除しました。</span><br>
			@endif
			予約確認ページです。<br>予約を修正する場合は、既存の予約を削除したあと新規の予約を追加してください。</p></div>
		<section class="calender_title">
			
		</section>
		<section class="book_form">
			@foreach ($book as $key => $value)
			<form method="post" action="{{ url('/reservation') }}">
				{{ csrf_field() }}
				<div class="reservation_area">
					<div class="column"><div class="column_name">日時：</div><div class="column_text">{{ substr($book[$key]['schedule_id'],0,4) }}年{{ substr($book[$key]['schedule_id'],5,2) }}月{{ substr($book[$key]['schedule_id'],8,2) }}日 {{ substr($book[$key]['book_time'],0,5) }}</div></div>
					<div class="column"><div class="column_name">予約ID：</div><div class="column_text">
					0000{{$book[$key]['book_id']}}</div></div>
					<div class="column"><div class="column_name">備考：</div><div class="column_text">
					{{$book[$key]['note']}}</div></div>
				</div>
				<div class="input_area">
					<input type="hidden" name="bookID" value="{{$book[$key]['book_id']}}">
					<input type="submit" name="cancel" class="cancel" value="取消">
				</div>
			</form>
			@endforeach
		</section>
	</div>

	@endsection

	<script type="text/javascript" src="../js/jquery.js"></script>
	<script type="text/javascript">
		
		var book = @json($book);
		console.log(book);

		$(window).ready(function(){
			$('.cancel').click(function(){

				var i = $('.cancel').index(this);

				var year = book[i]['schedule_id'].substr(0,4);

				var month = book[i]['schedule_id'].substr(5,2);

				var day = book[i]['schedule_id'].substr(8,2);

				var time = book[i]['book_time'].substr(0,5);

				var reserveID = '0000'+book[i]['book_id'];

				var note = book[i]['note'];

				if (note===null) {
					note = '';
				}

				var conf = confirm('この予約をキャンセルしてもよろしいいですか？\n日時：'+year+'年'+month+'月'+day+'日 '+time+'\n予約ID：'+reserveID+'\n備考：'+note);

				if (conf) {
					return true;
				}else{
					return false;
				}
			})
		});


	</script>
