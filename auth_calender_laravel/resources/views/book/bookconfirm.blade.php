@extends('layouts.app')


<!DOCTYPE html>
<html lang="ja">
<head>
	<title>Reserve</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/base.css">
	<link rel="stylesheet" type="text/css" href="../css/index.css">
	<link rel="stylesheet" type="text/css" href="../css/confirm.css">
</head>
<body>
@section('content')
	<div class="container">
		<h1>Reserve</h1>
		<div class="text"><p>ご予約の内容を確認し、間違いなければ予約ボタンを押してください。<br><span class="err_message">
			@foreach ($errors->all() as $error)
			{{$error}}<br>
			@endforeach</span></p></div>
		<section class="calender_title">
			<div class="month_body"><span class="year">{{ substr($request->id, 0, 4) }}</span>年<br><span class="month">{{ substr($request->id, 4, 2) }}</span>月<span class="month">{{ substr($request->id, 6, 2) }}</span>日</div>
		</section>
		<section class="book_form">
			<form method="post" action="{{ action('schedulesController@store',[$request->id]) }}">
				{{ csrf_field() }}
				<div class="time_area">
					<label class="form_label one">NAME：</label>
					<input type="hidden" name="name" required value="{{ $request->name }}">{{ $request->name }}<br>

					<label class="form_label two">TEL：</label>
					<input type="hidden" name="tel" required value="{{ $request->tel }}">{{ $request->tel }}<br>

					<label class="form_label three">TIME：</label>
					<input type="hidden" name="book_time" value="{{ $request->book_time }}" required><div class="time">{{ $request->book_time }}</div><br>
					<label class="form_label four">NOTE：</label>
					<input type="hidden" name="note" value="{{ $request->note }}">{{ $request->note }}
					<input type="hidden" name="id" value="{{ $request->id }}">
				</div>

				<input type="submit" name="submit" value="予約する">
				<input type="button" name="back" id="back" onClick="history.go(-1);"　value="戻る">
			</form>
		</section>
	</div>

	@endsection

	<script type="text/javascript" src="../js/jquery.js"></script>
<!-- </body>
</html> -->