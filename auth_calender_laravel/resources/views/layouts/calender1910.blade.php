@section('calender1910')
<?php

    $thisYear = (int) $thisYear;
    $thisMonth = (int) $thisMonth;

    $prevMonth = $thisMonth - 1;
    $nextMonth = $thisMonth + 1;
    $prevYear = $thisYear;
    $nextYear = $thisYear;

    if ($thisMonth == 1) {
        $prevMonth = 12;
        $prevYear = $thisYear - 1;
    }else if ($thisMonth == 12){
        $nextMonth = 1;
        $nextYear = $thisYear + 1;
    }

    $prevMonthDays = cal_days_in_month(CAL_GREGORIAN, $prevMonth, $prevYear);

    $thismonthDays = cal_days_in_month(CAL_GREGORIAN, $thisMonth, $thisYear);

    $firstDay = date('w',mktime(0,0,0,$thisMonth,1,$thisYear));

    $lastDay = date('w',mktime(0,0,0,$thisMonth,$thismonthDays,$thisYear));

    $prevDays = $firstDay - 1;

    $nextDays = 7 - $lastDay;

    // 最初の曜日が何曜日かで処理を分ける
    if ($firstDay == 0) {
        $prevDays = 6;
    }

    if ($lastDay == 0) {
        $nextDays = 0;
    }

    // $book
    $books =[];
    $bookTimes = [];
    $booked = [];
    for ($i = 0 ; $i < count($book); $i++) {
        $books[$i] = $book[$i]["schedule_id"];
        $bookTimes[$i] = $book[$i]["book_time"];
    }

    foreach($books as $key => $value){
        $booked[$key] = substr($value,0,4).substr($value,5,2).substr($value,8,2);
    }

?>

<section class="calender_title"><span class="year"><?php echo $thisYear; ?></span>年</section>
        <section class="calender_title">
            <div class="prev"><a href="?id={{ $prevYear }}{{ sprintf('%02d', $prevMonth) }}">prev<<</a></div>
            <div class="month_body"><span class="month"><?php echo $thisMonth; ?></span>月</div>
            <div class="next"><a href="?id={{ $nextYear }}{{  sprintf('%02d', $nextMonth)  }}">>>next</a></div>
        </section>
        <section class="calender_body">
            <div class="calender_frame">
                <div class="dayofweek top">
                    <p>月</p>
                </div>
                <div class="dayofweek top">
                    <p>火</p>
                </div>
                <div class="dayofweek top">
                    <p>水</p>
                </div>
                <div class="dayofweek top">
                    <p>木</p>
                </div>
                <div class="dayofweek top">
                    <p>金</p>
                </div>
                <div class="dayofweek top">
                    <p>土</p>
                </div>
                <div class="dayofweek top">
                    <p>日</p>
                </div>
        @for ($i = $prevMonthDays - $prevDays + 1; $i <= $prevMonthDays; $i++)
                <div class="oneday">
                    <div class="day_field gray"><span class="daynum" id="day{{sprintf('%02d', $prevMonth)}}{{sprintf('%02d', $i)}}">{{$prevMonth}}/{{$i}}</span></div>
                    <div class="free_field"></div>
                </div>
        @endfor
        @for ($i = 1; $i <= $thismonthDays; $i++)
                <div class="oneday">
                    <div class="day_field"><span class="daynum" id="day{{sprintf('%02d', $thisMonth)}}{{sprintf('%02d', $i)}}">{{$i}}</span>日</div>
                    <div class="free_field">
                    <?php
                        $thisMonthId = (string) sprintf('%02d', $thisMonth);
                        $dayId = (string) sprintf('%02d', $i);
                        $yearId = (string) $thisYear;
                        $countText = $yearId.$thisMonthId.$dayId;
                        $countArrey = array_keys($booked,$countText);
                        $countId = count($countArrey);
                    ?>
                    @if ($countId == 0)
                        <span class="poss" id="{{$thisYear}}{{sprintf('%02d', $thisMonth)}}{{sprintf('%02d', $i)}}">○</span>
                    @elseif ($countId == 1)
                        <span class="poss red" id="{{$thisYear}}{{sprintf('%02d', $thisMonth)}}{{sprintf('%02d', $i)}}">△</span>
                    @elseif ($countId == 2)
                        <span class="unposs" id="non{{$thisYear}}{{sprintf('%02d', $thisMonth)}}{{sprintf('%02d', $i)}}">✖</span>
                    @endif
                    </div>
                </div>
        @endfor
        @for ($i = 1; $i <= $nextDays; $i++)
                <div class="oneday">
                    <div class="day_field gray"><span class="daynum" id="day{{sprintf('%02d', $nextMonth)}}{{sprintf('%02d', $i)}}">{{$nextMonth}}/{{$i}}</span></div>
                    <div class="free_field"></div>
                </div>
        @endfor
            </div>
        </section>

@show
