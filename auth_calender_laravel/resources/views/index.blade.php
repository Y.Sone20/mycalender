@extends('layouts.app')


<!DOCTYPE html>
<html lang="ja">
<head>
	<title>MyCalender</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="css/base.css">
	<link rel="stylesheet" type="text/css" href="css/index.css">
</head>
<body>
@section('content')
	<div class="container">
        <h1>MyCalender</h1>
        <div class="text"><p>ご希望の日にちを選択すると、予約画面へ進みます。</p></div>
		@include('layouts.calender')
	</div>

	@endsection

	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/button.js"></script>
	