<?php

namespace Tests\Feature;
use App\Book;
use App\User;
use App\Http\Controllers\schedulesController;
use Illuminate\Support\Facades\Auth;//いらないかも
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class bookTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_book_controller()
    {
        $this->store();
    }

    private function store(){

        $user = factory(User::class)->create();

        $response = $this->actingAs($user);

        $date = [
            "book_time" => "15:00",
            "name" => "testuser",
            "tel" => 00000000000,
            "note" => "test",
            "id" => 20190101,
        ];

        $this -> post('/reserve/confirmed',$date)
         ->assertSuccessful();

        $this->assertDatabaseHas('books', [
          'user_id' => $user->id,
        ]);

        Book::where('user_id',$user->id)->delete();
        User::where('id',$user->id)->delete();
    }
}
