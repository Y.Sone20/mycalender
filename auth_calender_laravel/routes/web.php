<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/reserve', 'schedulesController@index');
Route::get('/reservation', 'ReserveController@reservation');
Route::post('/reservation', 'ReserveController@cancel');
Route::post('/reserve/confirm', 'schedulesController@confirm');
Route::post('/reserve/confirmed', 'schedulesController@store');
Route::get('/reserve/confirm', function(){
	return redirect("/home");
});
Route::get('/reserve/confirmed', function(){
	return redirect("/home");
});